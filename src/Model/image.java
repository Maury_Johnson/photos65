package Model;

import java.io.Serializable;
import java.util.ArrayList;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;


/**
 * 
 * @author linhdang
 *
 */
public class image implements Serializable {
	/**
	 * serial number
	 */
	private static final long serialVersionUID = 121214214214L;
	/**
	 * delacre width and height of the picture
	 */
	private int width, height;
	/**
	 * an 2D array that store data of the picture
	 */
	private int[][]data;
	/**
	 * a constructor that takes no args
	 */
	public image() {
		
		
	}
	/**
	 * Setting image by saving image data into 2D data array
	 * @param image take a image as paramater
	 */
	public void SetImage(Image image) {
		width = (int) image.getWidth();
		height = (int) image.getHeight();
		data = new int[width][height];
		PixelReader r = image.getPixelReader();
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				data[i][j] = r.getArgb(i, j);
			}
		}
	}
	/**
	 * Get image
	 * @return a serialized image
	 */
	public Image getImage() {
		WritableImage  image = new WritableImage(width,height);
		PixelWriter pixel = image.getPixelWriter();
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
					pixel.setArgb(i, j, data[i][j]);
			}
		}
		
		System.out.printf("\nSerialized Image:[%d,%d]->%s\n", width,height,image);
		
		return image;
	}
	

	
}
