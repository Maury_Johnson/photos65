package Model;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * 
 * @author linhdang
 *
 */
public class Album implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 133333L;
	public String name;
	public ArrayList<Photo> photolist;
	/**
	 * Album constructor
	 * @param name album name
	 */
	public Album(String name) {
		this.name = name;
		photolist = new ArrayList<Photo>();
	}
	/**
	 * get album name
	 * @return album name
	 */
	public String getName() {
		return name;
	}
	/**
	 * override to string method
	 */
	public String toString() {
		return name;
	}
	/**
	 * Edit album name
	 * @param newname new name of the album
	 */
	public void EditName(String newname) {
		name = newname;
	}
	/**
	 * get Array List of Photo
	 * @return photo list
	 */
	public ArrayList<Photo> getPhotoList(){
		return photolist;
	}

}
