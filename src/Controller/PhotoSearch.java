package Controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import Model.Album;
import Model.Photo;
import Model.User;

import Model.Tag;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * 
 * @author linhdang
 *
 */
public class PhotoSearch {
	/**
	 * Photo Search button
	 */
	@FXML
	Button SearchButton;
	/**
	 * Back Button
	 */
	@FXML
	Button BackButton;
	/**
	 * Log Out Button
	 */
	@FXML
	Button LogOutButton;
	/**
	 * Create Album Button
	 */
	@FXML
	Button CreateAlbumButton;

	/**
	 * Date Entry 1
	 */
	@FXML
	DatePicker DateEntry1;
	/**
	 * Typed Date Entry 1
	 */
	static String TypedDate1;

	/**
	 * Date Entry 2
	 */
	@FXML
	DatePicker DateEntry2;
	/**
	 * Typed Date Entry 2
	 */
	static String TypedDate2;

	/**
	 * TagField
	 */
	@FXML
	TextField TagField;

	/**
	 * Photo Collection
	 */
	@FXML
	TilePane PhotoCollection;
	
	/**
	 * Photo List
	 */
	@FXML
	ListView<Photo> PhotoList;
	
	/**
	 * Album Name TextField
	 */
	@FXML
	TextField AlbumName;
	
	/**
	 * Current User
	 */
	public User MyUser;
	
	/**
	 * Current User
	 */
	private User CurrentUser;
	
	/**
	 * Tag Array
	 */
	public String[] TagArray = null;
	// Stage level 2
	
	/**
	 * Indicates if tag is obtained
	 */
	boolean TagGo;
	/**
	 * Indicates if using AND expression
	 */
	boolean AND;
	/**
	 * Indicates if using OR expression
	 */
	boolean OR;
	/**
	 * Indicated if Date is obtained
	 */
	boolean DateGo;
	/**
	 * The Tag Input
	 */
	String TagInput;

	/**
	 * Start PhotoSearch Window
	 * @param S the scene to set window
	 * @param currentuser the current user
	 */
	public void start(Scene S, User currentuser) {

		if (MyUser == null) {
			System.err.println("HAVE NO USER FOR PHOTO SEARCH PhotoSearch.START()");
			return;
		}
		CurrentUser = currentuser;
		PhotoList.setCellFactory(new Callback<ListView<Photo>, ListCell<Photo>>() {
			@Override
			public ListCell<Photo> call(ListView<Photo> photoList) {
				return new ImageList();
			}
		});
		Controller.AddStage(2);

		Stage MyStage = Controller.Stages.get(2);

		MyStage.setOnCloseRequest(event -> {
			Controller.Stages.get(2).close();
			Controller.Serialization(Controller.Users);
			Controller.Stages.get(0).close();

		});

		MyStage.setScene(S);
		MyStage.showAndWait();
	}

	/**
	 * Function handles user eror cases
	 * @param errno indicates various error cases
	 */
	private void InvalidParam(int errno) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Validate Fields");
		alert.setHeaderText(null);
		if (errno == 1) {
			alert.setContentText("Field Empty ! Please Try Again !");
		} else if (errno == 2) {
			alert.setContentText("Date Field Empty ! Please Try Again !");
		} else if (errno == 3) {
			alert.setContentText("Username Does Not Exist!");
		} else if (errno == 4) {
			alert.setContentText("Invalid Field ! Please Try Again !");
		}else if (errno == 5) {
			alert.setContentText("Photo Search Result is empty!");
		}else if (errno == 6) {
			alert.setContentText("This album name already exists ! Please Try Again");
		}
		alert.showAndWait();
	}

	/**
	 * Handles creating an album
	 */
	public void HandleCreateAlbum() {
		if(AlbumName.getText().equals("")) {
			InvalidParam(1);
			return;
		}
		if(PhotoList.getItems().isEmpty()) {
			InvalidParam(5);
			return;
		}
		for(Album a: CurrentUser.getAlbum()) {
			if(a.getName().equals(AlbumName.getText())) {
				InvalidParam(6);
				return;
			}
		}
		Alert alert = new Alert(AlertType.CONFIRMATION);

		alert.setTitle("Comfirmation Dialog");
		alert.setHeaderText("Are you sure to Add?");
		Optional<ButtonType> action = alert.showAndWait();
		if (action.get() == ButtonType.CANCEL) {

			return;
		}
		if (action.get() == ButtonType.OK) {
			Album newalbum = new Album(AlbumName.getText());
			CurrentUser.getAlbum().add(newalbum);
			for(Photo p: PhotoList.getItems()) {
				newalbum.getPhotoList().add(p);
			}
			AlbumName.clear();
			Controller.Serialization(Controller.Users);

		}
		
		
	}

	/**
	 * Handle button clicks
	 * @param event Action taken
	 * @throws IOException Indicates Error thrown if one occurs
	 */
	public void convert(ActionEvent event) throws IOException {
		Button b = (Button) event.getSource();

		if (b == SearchButton) {
			HandleSearch();
		} else if (b == BackButton) {
			System.out.println("Back Button Clicked");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/User.fxml"));
			Parent parent = (Parent) loader.load();
			Scene scene = new Scene(parent);

			UserLoginController controller = loader.<UserLoginController>getController();
			controller.ShowAlbum(CurrentUser);
			controller.SetStage();

			Controller.Serialization(Controller.Users);
			Stage stage = (Stage) b.getScene().getWindow();
			stage.setScene(scene);
			Controller.Stages.get(2).close();
			controller.SetStage();
			
			stage.show();
			
			
		} else if (b == LogOutButton) {
			System.out.println("LogOut Button Clicked");
			Controller.Stages.get(2).close();
			Controller.Stages.get(0).show();
		} else if (b == CreateAlbumButton) {
			System.out.println("CreateAlbum Button Clicked");

			HandleCreateAlbum();
		}
	}

	/**
	 * Change The formatting of the tag
	 */
	public void ChangeTagFormat() {
		if (OR || AND) {
			if (TagArray.length != 2) {
				InvalidParam(4);
				return;
			}
			TagArray[0] = TagArray[0].substring(0, TagArray[0].indexOf("=")) + "-"
					+ TagArray[0].substring(TagArray[0].indexOf("=") + 1);
			TagArray[1] = TagArray[1].substring(0, TagArray[1].indexOf("=")) + "-"
					+ TagArray[1].substring(TagArray[1].indexOf("=") + 1);
			System.out.println(TagArray[0] + "," + TagArray[0]);
			System.out.println(TagArray[1] + "," + TagArray[1]);
		} else {
			TagInput = TagInput.substring(0, TagInput.indexOf("=")) + "-"
					+ TagInput.substring(TagInput.indexOf("=") + 1);
			System.out.println(TagInput);
		}

	}

	/**
	 * Handle Photo Search algorithm
	 */
	public void HandleSearch() {
		String tagText = TagField.getText();
		PhotoList.getItems().clear();
		AND = false;
		OR = false;
		System.out.println(tagText);
	
		TagGo = false;
	

		ArrayList<Album> albumList = CurrentUser.getAlbum();

		for (Album album : albumList) {
			ArrayList<Photo> photolist = album.getPhotoList();
			for (Photo photo : photolist) {

				ArrayList<Tag> taglist = photo.Taglist;

				if (DateEntry1.getValue() != null && DateEntry2.getValue() == null) {
					InvalidParam(2);
					return;
				}
				if (DateEntry1.getValue() == null && DateEntry2.getValue() != null) {
					InvalidParam(2);
					return;
				}

				if (DateEntry1.getValue() != null && DateEntry2.getValue() != null) {
					TagGo = true;
					if (photo.isWithinDateRange(DateEntry1.getValue(), DateEntry2.getValue())) {

						if (PhotoList.getItems().contains(photo)) {
							continue;
						} else {
							PhotoList.getItems().add(photo);
							PhotoList.refresh();
							
						}

					}
				}

				if (!tagText.equals("") && TagGo == false && taglist.size() > 0) {
					if (!tagText.contains("=")) {
						InvalidParam(4);
						return;
					}
					if (tagText.contains("OR")) {
						TagArray = tagText.split(" OR ");
						OR = true;

					} else if (tagText.contains("AND")) {
						TagArray = tagText.split(" AND ");
						AND = true;

					}
					TagInput = tagText;
					ChangeTagFormat();
					int count = 0;
					for (Tag t : taglist) {

						if (AND) {

							if (TagArray[0].equals(t.toString())) {
								count++;

							}
							if (TagArray[1].equals(t.toString())) {
								count++;
							}
						} else if (OR) {
							if (TagArray[0].equals(t.toString())) {
								if (PhotoList.getItems().contains(photo)) {
									continue;
								} else {
									PhotoList.getItems().add(photo);
									PhotoList.refresh();

								}
							}
							if (TagArray[1].equals(t.toString())) {
								if (PhotoList.getItems().contains(photo)) {
									continue;
								} else {
									PhotoList.getItems().add(photo);
									PhotoList.refresh();

								}
							}

						} else {
							if (TagInput.equals(t.toString())) {
								if (PhotoList.getItems().contains(photo)) {
									continue;
								} else {
									PhotoList.getItems().add(photo);
									PhotoList.refresh();
									break;
								}
							}
						}
					}

					if (count == 2) {
						if (PhotoList.getItems().contains(photo)) {
							continue;
						} else {
							PhotoList.getItems().add(photo);
							PhotoList.refresh();

						}

					}

				}

			}
		}
		TagField.clear();
		DateEntry1.getEditor().clear();
		DateEntry1.setValue(null);
		DateEntry2.getEditor().clear();
		DateEntry2.setValue(null);
		
		

	}

}