package Controller;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

import Model.Photo;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import Model.*;
/**
 * 
 * @author linhdang,Maury Johnson
 *
 */
public class AlbumController {

	/**
	 * Caption Text and Tag Text
	 */
	@FXML
	TextField CaptionText, TagText;
	/**
	 * Tag Value Text
	 */
	@FXML
	TextField TagValue;
	/**
	 * Add Photo button
	 */
	@FXML
	Button AddPhoto;
	/**
	 * View Photo Button
	 */
	@FXML
	Button ViewPhoto;
	/**
	 * Delete Photo Button
	 */
	@FXML
	Button DeletePhoto;
	/**
	 * Copy Move Button
	 */
	@FXML
	Button CopyMove;
	/**
	 * Logout button
	 */
	@FXML
	Button LogOut;
	/**
	 * Back Button
	 */
	@FXML
	Button Back;
	/**
	 * Add Caption Button
	 */
	@FXML
	Button AddCaption;
	/**
	 * DeleteCaption Button
	 */
	@FXML
	Button DeleteCaption;
	/**
	 * Edit Caption Button
	 */
	@FXML
	Button EditCaption;
	/**
	 * Add Tag Button
	 */
	@FXML
	Button AddTag;
	/**
	 * Delete Tag Button
	 */
	@FXML
	Button DeleteTag;
	/**
	 * Username Label
	 */
	@FXML
	Label UserName;

	/**
	 * PhotoList Button
	 */
	@FXML
	ListView<Photo> PhotoList;
	/**
	 * Open Photo Button
	 */
	@FXML 
	ImageView OpenPhoto;
	/**
	 * Previous button
	 */
	@FXML
	Button Previous;
	/**
	 * Next Button
	 */
	@FXML
	Button Next;

	/**
	 * Less data needed for Photo Detail
	 */
	@FXML
	ListView<String> PhotoDetail;
	
	/**
	 * Current User field
	 */
	@SuppressWarnings("unused")
	private User currentuser;

	/**
	 * UserList Field
	 */
	private ArrayList<User> UserList;

	/**
	 * 
	 */
	//private ArrayList<Photo> photoAr = new ArrayList<Photo>();

	//private Desktop desktop = Desktop.getDesktop();

	/**
	 * Stage field for Current Stage
	 */
	public Stage stage;
	
	/**
	 * Album Field for Current Album
	 */
	public Album album;

	/**
	 * PhotoSelected Field
	 */
	private Photo PhotoSelected;

	/**
	 * Parameter indicating no field is selected
	 */
	private static final int NOFIELDSELECTED = 1;
	/**
	 * Parameter indicating no tag is entered
	 */
	private static final int NOTAGENTRY = 2;
	/**
	 * Parameter indicating special tostring method
	 */
	private static final int TOSTRINGMETHODTWO = -214774854;
	/**
	 * Parameter indicating invalid duplicate tag
	 */
	private static final int INVALIDDUPLICATETAG = 3;
	/**
	 * Parameter indicating invalid caption entry
	 */
	private static final int INVALIDCAPTIONENTRY = 4;
	/**
	 * Parameter indicating invalid caption entry
	 */
	private static final int NOCAPTIONENTRY = 5;
	/**
	 * Parameter indicating the tag does not exist
	 */
	private static final int TAGDOESNOTEXIST = 6;
	/**
	 * Parameter inicating invalid duplicate caption
	 */
	private static final int INVALIDDUPLICATECAPTION = 7;
	/**
	 * Parameter indicating bad input
	 */
	private static final int BADINPUT = -1;
	/**
	 * Parameter indicating Bad Tag
	 */
	private static final int BADTAG = -2;

	// Stage level 2
	/**
	 * Start Album Controller
	 * @param S Starting Parameter for Album
	 */
	public void start(Scene S) {
		Controller.AddStage(2);

		Stage MyStage = Controller.Stages.get(2);

		MyStage.setOnCloseRequest(event -> {
			System.out.println("Closing PhotoSearch Stage");
			// ON X GO BACK??
			// Controller.Stages.get(Controller.Stages.size()-2).show();
			// OR LOGIN wINDOW
			Controller.Stages.get(2).close();
			Controller.Serialization(Controller.Users);
			Controller.Stages.get(0).close();
		});

		this.PhotoList.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<Photo>() {
			public void onChanged(ListChangeListener.Change<? extends Photo> c) {
				// Photo.toString...
				String[] U1 = c.toString().split(" replaced by ");
				String Photo = U1.length > 1 ? U1[1].substring(1, U1[1].indexOf("]"))
						: U1[0].substring(U1[0].indexOf("[") + 1, U1[0].indexOf("]"));
				System.out.printf("\n%s Photo Selected: %s\n", c, Photo);

				List<Photo> MyPhoto = GetPhotoFromView(Photo);

				if (MyPhoto.size() > 0)
					ViewPhotoDetails(MyPhoto.get(0));
			}
		});

		MyStage.setScene(S);
		MyStage.showAndWait();
	}

	/**
	 * Get Photo From View
	 * @param Photo String photo used for getting matching photo from view
	 * @return Return A List of Photos
	 */
	public List<Photo> GetPhotoFromView(String Photo) {
		return (PhotoList.getItems().stream().filter((P) -> P.toString().equals(Photo))).collect(Collectors.toList());
	}

	/**
	 * View Photo's Details, package level in order to be accessed from listener
	 * 
	 * @param Photo photo to use to view its details
	 */
	void ViewPhotoDetails(Photo Photo) {
		PhotoDetail.getItems().clear();
		System.out.println(Photo.toString(TOSTRINGMETHODTWO));
		PhotoDetail.getItems().add(Photo.toString(TOSTRINGMETHODTWO));
		PhotoSelected = Photo;
	}

	/**
	 * View All photo of current album
	 * @param user current user
	 * @param album current album
	 */
	public void ViewPhotoList(User user, Album album) {
		UserList = Controller.Users;
		currentuser = user;
		this.album = album;

		PhotoList.setCellFactory(new Callback<ListView<Photo>, ListCell<Photo>>() {
			@Override
			public ListCell<Photo> call(ListView<Photo> photoList) {
				return new ImageList();
			}
		});

		PhotoList.setItems(FXCollections.observableArrayList(album.getPhotoList()));
		if(album.getPhotoList().size() >  0) {
		PhotoList.getSelectionModel().select(0);
		Photo selectedphoto = PhotoList.getSelectionModel().getSelectedItem();
		ViewPhotoDetails(selectedphoto);
		OpenPhoto.setImage(selectedphoto.getImage());
		}
		
		
		UserName.setText(user.getName() + "'s Album\n" + "Album Name: " + album.getName());
	}

	/**
	 * Handle Back Button
	 * @param back back button
	 */
	public void HandleBack(Button back) {

		try {
			Controller.Stages.get(2).close();
			Controller.Serialization(Controller.Users);
			Controller.Stages.get(1).show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Check if photo is duplicated
	 * @param photo photo to check if duplicated in Photo List
	 * @return boolean value indicating if photo is duplicated in Photo list
	 */
	public boolean isDuplicatedPhoto(Photo photo) {
		if (album.getPhotoList().size() > 0) {
			for (int i = 0; i < album.getPhotoList().size(); i++) {
				if (album.getPhotoList().get(i).equals(photo)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Handle Add Photo
	 * 
	 * @throws MalformedURLException a
	 */
	public void HandleAddPhoto() throws MalformedURLException {

		stage = new Stage();

		final FileChooser fileChooser = new FileChooser();
		configureFileChooser(fileChooser);
		File file = fileChooser.showOpenDialog(stage);
		if (file != null) {
			// openFile(file);
			System.out.println("long last modified"+file.lastModified());
			String imagepath = file.toURI().toURL().toString();
			Image image = new Image(imagepath);
			Calendar date = Calendar.getInstance();

			Photo photo = new Photo(file.getName(), imagepath, image, date,file.lastModified());

			Alert alert = new Alert(AlertType.CONFIRMATION);

			alert.setTitle("Comfirmation Dialog");
			alert.setHeaderText("Are you sure to Add?");
			Optional<ButtonType> action = alert.showAndWait();
			if (action.get() == ButtonType.CANCEL) {

				return;
			}
			if (action.get() == ButtonType.OK) {
				AddPhoto(photo);

			}
		}
	}

	/**
	 * Save data
	 * 
	 * @param userlist User List
	 */
	public void Serialization(ArrayList<User> userlist) {
		try {

			FileOutputStream file = new FileOutputStream(System.getProperty("user.dir") + "/" + "UserAlbum.ser");
			ObjectOutputStream out = new ObjectOutputStream(file);

			out.writeObject(userlist);

			out.close();
			file.close();

			System.out.println("Object has been serialized");

		}

		catch (IOException ex) {
			System.out.println("IOException is caught");
		}
	}
	/**
	 * Add Photo
	 * @param photo new photo
	 */
	public void AddPhoto(Photo photo) {
		if (isDuplicatedPhoto(photo)) {
			Controller.InValidEntry(Controller.DuplicatedPhoto);
			return;
		}
		PhotoList.getItems().add(photo);
		album.getPhotoList().add(photo);
		PhotoList.getSelectionModel().select(photo);
		Serialization(Controller.Users);
	}
	/**
	 * File chooser Figure
	 * @param fileChooser chosen file
	 */
	private static void configureFileChooser(final FileChooser fileChooser) {
		fileChooser.setTitle("View Pictures");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All Images", "*.*"),
				new FileChooser.ExtensionFilter("JPG", "*.jpg"), new FileChooser.ExtensionFilter("PNG", "*.png"));
	}

	/**
	 * Invalid parameter cases, gives alertbox
	 * 
	 * @param errno erro number
	 */
	private void InvalidParam(int errno) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Validate Fields");
		alert.setHeaderText(null);
		if (errno == BADTAG) {
			alert.setContentText("Bad Tag Selected ! Please Try Again !");
		}
		if (errno == BADINPUT) {
			alert.setContentText("Bad Tag Input ! Please Try Again !");
		}
		if (errno == 1) {
			alert.setContentText("No Photo Selected ! Please Try Again !");
		}
		if (errno == 2) {
			alert.setContentText("No Tag Entry ! Please Try Again !");
		}
		if (errno == 3) {
			alert.setContentText("Invalid Tag Entry ! Example tag entry-> person:MJ Please Try Again !");
		}
		if (errno == 4) {
			alert.setContentText("No Caption Entry ! Please Try Again !");
		}
		if (errno == 5) {
			alert.setContentText("Invalid Caption Entry ! Please Try Again !");
		}
		if (errno == 6) {
			alert.setContentText("Tag Entry Does Not Exist ! Please Try Again !");
		}
		if (errno == 7) {
			alert.setContentText("Entry Does Already Exists ! Please Try Again !");
		}
		if (errno == 1000) {
			alert.setContentText("Please Select Photo!");
		}
		alert.showAndWait();
	}
	/**
	 * Handle Log out button 
	 * @param b button
	 */
	public void HandleLogOut(Button b) {

		PopUpPane(b);

	}
	/**
	 * Pop up pane
	 * @param b button
	 */
	public void PopUpPane(Button b) {

		try {

			Controller.Stages.get(2).close();
			Controller.Serialization(Controller.Users);
			Controller.Stages.get(0).show();

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
/**
 * Handle open photo
 */
	public void HandleViewPhoto() {
		Photo item = PhotoList.getSelectionModel().getSelectedItem();
		OpenPhoto.setImage(item.getImage());
	
	}
	
	/**
	 * Handle switching photos
	 * @param b Button to handle switching photo
	 */
	public void HandleSwitch(Button b) {
		int index = PhotoList.getSelectionModel().getSelectedIndex();
		int ListLength = PhotoList.getItems().size();
		System.out.println("currentindex1:" + index);
		System.out.println(ListLength);
		if (b == Next) {
			if(index < ListLength - 1 ) {
				index++;
				System.out.println("currentindex2:" + index);
			}else {
				index = 0;
			}
		} else {
			if (index > 0) {
				index--;
			} else {
				index = ListLength - 1;
			}
		}
		System.out.println("currentindex3:" + index);
		PhotoList.getSelectionModel().select(index);
		Photo photo = PhotoList.getSelectionModel().getSelectedItem();
		

		OpenPhoto.setImage(photo.getImage());

	}
	
/**
 * Hanlde all buttons of Open Album
 * @param e Action event
 * @throws MalformedURLException Throw an exception
 */
	public void HandleButton(ActionEvent e) throws MalformedURLException {
		Button b = (Button) e.getSource();
		if (b == AddPhoto) {
			HandleAddPhoto();
		} else if (b == ViewPhoto) {
			HandleViewPhoto();
		} else if (b == DeletePhoto) {
			HandleDeletePhoto();
		} else if (b == CopyMove) {
			HandleSubAlbum();
		} else if (b == LogOut) {
			HandleLogOut(b);
		} else if (b == Back) {
			HandleBack(b);
		} else if (b == AddCaption) {
			HandleAddCaption();
		} else if (b == EditCaption) {
			HandleEditCaption();
		} else if (b == AddTag) {
			HandleAddTag();
		} else if (b == DeleteTag) {
			HandleDeleteTag();
		}else if (b == Previous || b == Next) {
			HandleSwitch(b);
		}
	}
/**
 * Handle move and copy album pane
 */
	private void HandleSubAlbum() {
		String FXMLPath = "/View/SubAlbumList.fxml";
		int index = PhotoList.getSelectionModel().getSelectedIndex();
		if (index < 0) {
			InvalidParam(1000);
			return;
		}
		Photo photo = PhotoList.getSelectionModel().getSelectedItem();
		try {

			FXMLLoader loader = new FXMLLoader(getClass().getResource(FXMLPath));
			Parent parent = (Parent) loader.load();
			Scene scene = new Scene(parent);

			SubAlbumController controller = loader.<SubAlbumController>getController();
			controller.start(currentuser);
			controller.getPhotoList(PhotoList);
			controller.GetCurrentAlbum(album);
			controller.getSelectedPhoto(photo);

			Controller.Stages.get(2).hide();
			Controller.Serialization(Controller.Users);
			Controller.AddStage(3);
			Controller.Stages.get(3).setScene(scene);
			Controller.Stages.get(3).show();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	/**
	 * Handle deleting tag
	 */
	private void HandleDeleteTag() {
		// TODO Auto-generated method stub

		boolean Empty = false;

		String Tag = "";

		if (TagText.getText() != null) {
			if (TagText.getText().length() > 0) {
				if (PhotoSelected != null) {
					Tag += TagText.getText();

				} else {// AT THIS STAGE,...
						// CRITICAL ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					System.err.println("CRITICAL ERROR!!!!!!!!!!!!!!! HANDLE TAG");
				}

			

			} else {
				Empty = true;
				InvalidParam(NOTAGENTRY);
				return;
			}

		} else {
			Empty = true;
			InvalidParam(NOTAGENTRY);
			return;
		}
		
		System.out.printf("Tag so far:%s\n", Tag);

		Tag+="-";
		
		if (TagValue.getText() != null) {
			if (TagValue.getText().length() > 0) {
				if (PhotoSelected != null) {
					
					Tag += TagValue.getText();
					/*
					*/
				} else {// AT THIS STAGE,...
						// CRITICAL ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					System.err.println("CRITICAL ERROR!!!!!!!!!!!!!!! HANDLE TAG");
				}
			} else {
				if (Empty == true) {
					InvalidParam(NOTAGENTRY);
				}
			}
		} else {
			if (Empty == true) {
				InvalidParam(NOTAGENTRY);
			}
		}

		final String FinalTag = Tag;

		
		System.out.printf("FINAL TAG:%s", FinalTag);
		
		boolean Removed = PhotoSelected.Taglist.removeIf((T) -> T.toString().equals(FinalTag));

		if (!Removed) {
			InvalidParam(INVALIDDUPLICATETAG);
		} else {
			ViewPhotoDetails(PhotoSelected);
			Controller.Serialization(UserList);
		}

	}

	/**
	 * Handle Adding tag
	 */
	private void HandleAddTag() {
		// TODO Auto-generated method stub

		boolean Empty = false;

		String Tag = "";

		System.out.printf("\nTAG 2 VALUE:%s\n", TagValue.getText());

		List<Photo> MyPhoto = null;

		if (TagText.getText() != null) {
			if (TagText.getText().length() > 0) {
				// Good tag entry
				if (PhotoSelected != null) {
					// Photo is selected for tag
					MyPhoto = GetPhotoFromView(PhotoSelected.toString());

					if (MyPhoto.size() > 0) {
						// if (MyPhoto.get(0).NoDuplicateTagValuePair(TagParameters)) {
						Tag += TagText.getText();

						System.out.printf("\nAdd a tag1:%s", Tag);
						TagText.clear();

					}

					else {// AT THIS STAGE,...
							// CRITICAL ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						System.err.println("CRITICAL ERROR!!!!!!!!!!!!!!! HANDLE TAG NAME");
					}
				} else {
					Empty = true;
					InvalidParam(NOFIELDSELECTED);
					return;
				}
			} else {
				Empty = true;
				InvalidParam(NOTAGENTRY);
				return;
			}

			//Tag += "-";

		} else {
			Empty = true;
			InvalidParam(NOTAGENTRY);
			return;
		}

		
		Tag+="-";
		
		System.out.printf("ADD TAG SO FAR:%s\n",Tag);
		
		if (TagValue.getText() != null) {
			if (TagValue.getText().length() > 0) {
				// Good tag entry
				System.out.println("Good Tag Val");
				if (PhotoSelected != null) {
					// Photo is selected for tag
					MyPhoto = GetPhotoFromView(PhotoSelected.toString());

					if (MyPhoto.size() > 0) {

						Tag += TagValue.getText();
						System.out.printf("\nAdd a tag2: %s\n", TagValue.getText());
						TagValue.clear();

					}

					else {// AT THIS STAGE,...
							// CRITICAL ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						System.err.println("CRITICAL ERROR!!!!!!!!!!!!!!! HANDLE TAG VALUE");
					}
				} else {
					System.out.println("No FIELD SELECTED");
					if (Empty == true) {
						InvalidParam(NOFIELDSELECTED);
					}
					// InvalidParam(NOFIELDSELECTED);
				}
			} else {
				System.out.println("NO TAG");
				if (Empty == true) {
					InvalidParam(NOTAGENTRY);
				}
				// InvalidParam(NOTAGENTRY);
			}
		} else {
			System.out.println("NULL TAG");
			if (Empty == true) {
				InvalidParam(NOTAGENTRY);
			}
			// InvalidParam(NOTAGENTRY);
		}

		
		System.out.printf("ADD FINAL TAG SO FAR:%s\n",Tag);
		
		if (MyPhoto != null) {
			String[] T = new String[2];

			System.out.printf("TAG SPLIT LEN:%d\n", Tag.split("-").length);

			if (Tag.split("-").length < 2) {
				if (Empty) {
					T[1] = Tag.split("-")[1];
					T[0] = "";
				} else {
					T[0] = Tag.split("-")[0];
					T[1] = "";
				}
			} else {
				T[0] = Tag.split("-")[0];
				T[1] = Tag.split("-")[1];
			}

			int ret = MyPhoto.get(0).AddTag((T));

			if (ret < 0) {
				if (ret == BADTAG) {
					InvalidParam(BADTAG);
				} else if (ret == BADINPUT) {
					InvalidParam(BADINPUT);
				}
			}
			ViewPhotoDetails(MyPhoto.get(0));
		} else {
			System.err.println("CRITICAL ERROR!!!!!!!!!!!!!!! HANDLE TAG ADD END");
		}
	}

	/**
	 * Handle Editing Caption
	 */
	private void HandleEditCaption() {
		// TODO Auto-generated method stub
		if (CaptionText.getText() != null) {
			// if (CaptionText.getText().length() > 0) {
			// Have a caption to add
			if (PhotoSelected != null) {
				// Selected a photo
				List<Photo> MyPhoto = GetPhotoFromView(PhotoSelected.toString());
				if (MyPhoto.size() > 0) {
					if (!MyPhoto.get(0).Caption.equals(CaptionText.getText())) {
						MyPhoto.get(0).Caption = CaptionText.getText();
						CaptionText.clear();

					} else {
						InvalidParam(INVALIDDUPLICATECAPTION);
						return;
					}
					// View new photo details added
					ViewPhotoDetails(MyPhoto.get(0));
				} else {// AT THIS STAGE...
						// CRITICAL ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					System.err.println("CRITICAL ERROR!!!!!!!!!!!!!!! HANDLE ADD CAPTION");
				}
			} else {
				InvalidParam(NOFIELDSELECTED);
			}
			// } else {
			// InvalidParam(NOCAPTIONENTRY);
			// }
		} else {
			InvalidParam(NOCAPTIONENTRY);
		}
	}
	
	/**
	 * Handle Adding Caption
	 */
	private void HandleAddCaption() {
		// TODO Auto-generated method stub
		if (CaptionText.getText() != null) {
			if (CaptionText.getText().length() > 0) {
				// Have a caption to add
				if (PhotoSelected != null) {
					// Selected a photo
					List<Photo> MyPhoto = GetPhotoFromView(PhotoSelected.toString());
					if (MyPhoto.size() > 0) {
						if (!MyPhoto.get(0).Caption.equals(CaptionText.getText())) {
							MyPhoto.get(0).Caption = CaptionText.getText();
							CaptionText.clear();
						} else {
							InvalidParam(INVALIDDUPLICATECAPTION);
							return;
						}
						// View new photo details added
						ViewPhotoDetails(MyPhoto.get(0));
					} else {// AT THIS STAGE...
							// CRITICAL ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						System.err.println("CRITICAL ERROR!!!!!!!!!!!!!!! HANDLE ADD CAPTION");
					}
				} else {
					InvalidParam(NOFIELDSELECTED);
				}
			} else {
				InvalidParam(NOCAPTIONENTRY);
			}
		} else {
			InvalidParam(NOCAPTIONENTRY);
		}
	}

	/**
	 * Delete Photo
	 * @param item Photo to delete
	 */
	public void DeletePhoto(Photo item) {
		album.getPhotoList().remove(item);
		PhotoList.getItems().remove(item);
		PhotoList.refresh();
		PhotoList.getSelectionModel().select(0);
		Serialization(UserList);
	}

	/**
	 * Handle Deleting PHoto
	 */
	private void HandleDeletePhoto() {

		Alert alert = new Alert(AlertType.CONFIRMATION);

		alert.setTitle("Comfirmation Dialog");
		alert.setHeaderText("Are you sure to Add?");
		Optional<ButtonType> action = alert.showAndWait();
		Photo item = PhotoList.getSelectionModel().getSelectedItem();
		if (action.get() == ButtonType.CANCEL) {
			return;
		}
		if (action.get() == ButtonType.OK) {
			DeletePhoto(item);
		}
	}
}
