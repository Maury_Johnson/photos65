package Controller;

import Model.Photo;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

/**
 * 
 * @author linhdang
 *
 */
public class ImageList extends ListCell<Photo> {
	/**
	 * caption string
	 */
	Label caption;
	/**
	 * Label file name
	 */
	Label filename;
	/**
	 * label name of file name
	 */
	Label captionlabel;
	/**
	 * create an image view object
	 */
	ImageView imageView = new ImageView();
	/**
	 * Create an AnchorPane Object
	 */
	AnchorPane anchorpane = new AnchorPane();

	@SuppressWarnings("static-access")
	/**
	 * A Image List constructor that create a picture view for the list of the
	 * picture
	 */
	public ImageList() {
		super();
		filename = new Label();
		captionlabel = new Label();
		caption = new Label();
		StackPane pane = new StackPane();

		imageView.setFitHeight(45);
		imageView.setFitHeight(45);
		imageView.setPreserveRatio(true);

		imageView.setPreserveRatio(true);
		pane.setAlignment(imageView, Pos.CENTER);
		pane.getChildren().add(imageView);

		pane.setPrefHeight(45);
		pane.setPrefWidth(45);
		AnchorPane.setLeftAnchor(pane, 0.0);
		
		AnchorPane.setLeftAnchor(filename, 95.0);
		AnchorPane.setTopAnchor(filename, 0.0);
		AnchorPane.setLeftAnchor(captionlabel, 95.0);
		AnchorPane.setTopAnchor(captionlabel, 25.0);
		AnchorPane.setLeftAnchor(caption, 150.0);
		AnchorPane.setTopAnchor(caption, 25.0);
		
		anchorpane.getChildren().addAll(filename, caption, captionlabel, pane);
		anchorpane.setPrefHeight(60);

		setGraphic(anchorpane);

	}

	@Override
	/**
	 * Override updateItem method of ListCell<Photo>
	 * 
	 * @param photo a photo
	 * @param empty boolean value
	 */
	public void updateItem(Photo photo, boolean empty) {
		super.updateItem(photo, empty);
		setText(null);
		if (photo == null) {
			imageView.setImage(null);
			
			filename.setText("");
			captionlabel.setText("");
			caption.setText("");

		}
		if (photo != null) {
			imageView.setImage(photo.getImage());
		
			filename.setText(photo.toString());
			captionlabel.setText("Caption: ");
			caption.setText(photo.getCaption());
		}
	}

}
